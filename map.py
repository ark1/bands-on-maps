import random
import time
import bandsintown
import gmplot

DEFAULT_ARTISTS_FILE_NAME = 'artists.txt'
DEFAULT_RESULT_HTML_NAME = 'map.html'


def read_artist_names(filename=DEFAULT_ARTISTS_FILE_NAME):
    with open(filename, encoding='utf-8') as file:
        return list(map(str.strip, file))


def get_client():
    return bandsintown.Client(__name__)


def get_events(client, artist_names, **kwargs):
    return dict(zip(artist_names, map(lambda name: client.events(name, **kwargs),
                                      artist_names)))


def reformat_time(time_str):
    return time.strftime("%Y-%m-%d", time.strptime(time_str, "%Y-%m-%dT%X"))


def get_events_center(events):
    sum_lat, sum_lng, n = 0, 0, 0
    for events_of_artist in events.values():
        for event in events_of_artist:
            sum_lat += event['venue']['latitude']
            sum_lng += event['venue']['longitude']
            n += 1
    if n == 0:
        return 0, 0
    return sum_lat / n, sum_lng / n


def add_event_markers(events, gmap):
    artist_colors = set(gmap.html_color_codes.keys())
    for artist_name in events:
        color = random.sample(artist_colors, 1)[0]
        artist_colors.remove(color)
        for event in events[artist_name]:
            venue = event['venue']
            lat, lng = venue['latitude'], venue['longitude']
            gmap.marker(lat, lng,
                        title=artist_name + '\\n' + reformat_time(event['datetime']),
                        color=color)


b_client = get_client()
b_events = get_events(b_client, read_artist_names(), date="2018-01-30,2018-02-02")
# noinspection SpellCheckingInspection
g_map = gmplot.GoogleMapPlotter(*get_events_center(b_events), 2,
                                'AIzaSyA_clxX2Up1RGdX5HbMiG4_9HPLCpqxy6A')
add_event_markers(b_events, g_map)
print(len(g_map.points))
g_map.draw(DEFAULT_RESULT_HTML_NAME)
